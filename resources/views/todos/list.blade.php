@extends('layouts.app')
@section('content')
<div class="container">
    {{-- Ako vo sesijata ima vakava poraka, pokazi ja --}}
    @if(session()->has('message'))
    <div class="alert alert-success mt-3" role="alert">
     {{session()->get('message')}}
      </div>
    @endif
    <div class="d-flex justify-content-between">
  <h2>Todos</h2>
  <a class="btn btn-primary" href="{{ route('todos.create') }}">Add ToDo</a>
</div>
  <hr>
  <ul class="list-group">
      {{-- pravime foreach na todos od session --}}
    @foreach(session('todos') as $todo)
    <li class="list-group-item d-flex justify-content-between align-items-center">
        {{-- Link za samoto ToDo, slug go koristime kako ID --}}
      <a href="{{ route('todos.show', ['slug'=> $todo['slug']]) }}" aria-describedby="{{ $todo['slug'] }}">
       {{ $todo['title'] }}
    </a>
    <small id="{{ $todo['slug'] }}">{{ $todo['category'] }}</small>
      <form  action="{{ route('todos.delete',['slug'=> $todo['slug']]) }}" method="POST" class="badge badge-primary">
        @csrf
<button type="submit" class="btn btn-sm">
    <svg aria-hidden="true" class= "text-danger m-1" style="width:20px;height:20px;" focusable="false" data-prefix="fas" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash fa-w-14 fa-3x"><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z" class=""></path></svg>
</button>
    </form>
    </li>
    @endforeach
  </ul>
  {{-- {{ print_r(session()->all() )}} --}}
</div>

@endsection
