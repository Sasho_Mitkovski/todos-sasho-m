@extends('layouts.app')
@section('content')

<div class="container">

        <h3>Add new todo</h3>
            <a href="/todos">Back</a>
  <hr>
  <form action="{{ route('todos.store') }} " method="POST">
    @csrf
      <div class="form-group col-4">
          <label for="title">Title</label>
          <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}">
      </div>
      <div class="form-group col-4">
          <label for="category">Category </label>
          <input type="text" name="category" class="form-control" id="category" value="{{ old('category') }}">
      </div>
      <div class="form-group col-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>

  </form>
</div>
@endsection
