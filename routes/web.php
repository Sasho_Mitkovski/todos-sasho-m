<?php

use App\Http\Controllers\TodoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

// ToDo list todos, show todo, create todo, delete todo, update todo,
// Koga imame ruta so dinamicen vlezen parametar, istata treba da e najdolu vo listata na ruti
//  vazi za ruti so ist controller, i isto url
Route::get('/todos', [App\Http\Controllers\TodoController::class, 'index'])->name('todos.index');
Route::get('/todos/create', [TodoController::class, 'create'])->name('todos.create');
Route::post('/todos/store', [TodoController::class, 'store'])->name('todos.store');
Route::get('/todos/{slug}', [TodoController::class, 'show'])->name('todos.show');
Route::post('/todos/delete/{slug}', [TodoController::class, 'delete'])->name('todos.delete');
Route::post('/todos/update/{id}', [TodoController::class, 'update']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
