<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Cookie;
use illuminate\Support\Str;

class TodoController extends Controller
{
    // lista na todos
    public function index()
    {
        // session()->forget('todos');// za brisenj na session
        return view('todos.list');
    }
    // Pokazuvanje na edno todo
    public function show($slug)
    {
    }
    // Kreiranje na novo Todo
    public function create()
    {
        return view('todos.create');
    }
    // Zacuvuvanje na novo ToDo
    public function store(Request $request)
    {
        // ================  Moze na ovoj nacin ==============
        //    $validated =  $request->validate([
        //         'title' => 'required|min:5',
        //         'category' => 'required'
        //     ]);

        //     $request->session()->push('todos', $validated);
        //     dd($request->all());
        // return route('todos.store');

        //==================  Moze i na ovoj nacin ==========================
        $request->validate([
            'title' => 'required|min:5',
            'category' => 'required'
        ]);

        $request->session()->push('todos', [
            'slug' => Str::random(8), // Namesto ID, stavame slug so 8 karakteri
            'title' => $request->input('title'),
            'category' => $request->input('category')
        ]);

        // poraka za dodaden Todo, koja ke se pokaze vo list.blade.php
        // Flash porakata se printa samo ednas
        $request->session()->flash('message', 'Dodaden e nov todo');

        return redirect()->route('todos.index');
    }
    // Update na ToDo
    public function update()
    {
    }
    // Delete na ToDo
    public function delete($slug)
    {
        $todos = session('todos');        // Zemi gi site todos

        foreach($todos as $key=>$todo){   // napravi foreach na site todos
            if($todo['slug'] == $slug){   // najdo go slagot koj e ednakov na toj sto e pusten vo URL
                unset($todos[$key]);       //Izbrishi go
            }
        }
        session()->flash('message', "Vasheto todo e izbrishano");
        session(['todos' => $todos]);
        return redirect()->route('todos.index');
    }
}
